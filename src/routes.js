import { createStackNavigator } from 'react-navigation';
import { fromRight, fadeIn } from 'react-navigation-transitions';

import React, { BackHandler } from 'react-native';

import Main from './pages/main';
import Place from './pages/place';
import MapSelector from './pages/mapSelector';
import Success from './pages/success'

const handleCustomTransition = ({ scenes }) => {
  const prevScene = scenes[scenes.length - 2];
  const nextScene = scenes[scenes.length - 1];

  // Custom transitions go there
  if (prevScene
    && prevScene.route.routeName === 'Place'
    && nextScene.route.routeName === 'MapSelector') {
    return fadeIn();
  }
  return fromRight();
}

const navigator = createStackNavigator({
  Main,
  Place,
  MapSelector,
  Success
}, {
    navigationOptions: {
      headerStyle: {
        backgroundColor: "#FDD912",
      },
      headerTintColor: "#FFF",
    },
    transitionConfig: (nav) => handleCustomTransition(nav),
  });

export default navigator;

const prevGetStateForAction = navigator.router.getStateForAction;

navigator.router.getStateForAction = (action, state) => {
  // Do not allow to go back from Home
  if (action.type === 'Navigation/BACK' && state && state.routes[state.index].routeName !== "MapSelector") {
    if (state.routes[state.index].routeName === 'Main') {
      BackHandler.exitApp();
    }

    return null;
  }

  //   // Do not allow to go back to Login
  //   if (action.type === 'Navigation/BACK' && state) {
  //     const newRoutes = state.routes.filter(r => r.routeName !== 'Login');
  //     const newIndex = newRoutes.length - 1;
  //     return prevGetStateForAction(action, { index: newIndex, routes: newRoutes });
  //   }
  return prevGetStateForAction(action, state);
}; ''