import React, { Component } from 'react';
import people_api from "../services/people_api";

import { View, Text, TextInput, TouchableOpacity, StyleSheet, Platform } from 'react-native';
import { TextInputMask } from 'react-native-masked-text';

export default class Main extends Component {
  static navigationOptions = {
    title: "+Barato - Administração",
  }

  state = {
    name: "",
    last_name: "",
    cnpj: "",
    email: "",
    username: "",
    password: "",
    message: "",
  };

  createUser = async () => {
    try {
      if (
        this.state.first_name == "" ||
        this.state.last_name == "" ||
        this.state.cnpj == "" ||
        this.state.email == "" ||
        this.state.username == "" ||
        this.state.password == ""
      ) {
        this.setState({ message: "Você deve preencher todos os campos corretamente." });
        return;
      }

      const response = await people_api.post("/api/users", {
        first_name: this.state.name,
        last_name: this.state.last_name,
        username: this.state.username,
        password: this.state.password,
        email: this.state.email,
        document_type: "CNPJ",
        document: this.state.cnpj,
        role: {
          name: "User"
        },
        token: null
      });

      const { id } = response.data

      this.props.navigation.navigate("Place", {
        userId: id,
      });

      this.setState({
        name: "",
        last_name: "",
        cnpj: "",
        email: "",
        username: "",
        password: "",
        message: "",
      });
    }
    catch {
      this.setState({ message: "Falha na criação do usuário. Tente novamente." });
    }
  }

  changeName = (name) => {
    this.setState({ name: name });

    if (name.length === 0 && this.state.last_name.length > 0) {
      this.setState({ username: this.state.last_name.toLowerCase() });
      return;
    }

    this.setState({ username: this.state.last_name.length == 0 ? name.toLowerCase() : (name + "." + this.state.last_name).toLowerCase() });
  };

  changeLastName = (last_name) => {
    this.setState({ last_name: last_name });

    if (last_name.length === 0 && this.state.name.length > 0) {
      this.setState({ username: this.state.name.toLowerCase() });
      return;
    }

    this.setState({ username: this.state.name.length == 0 ? last_name.toLowerCase() : (this.state.name + "." + last_name).toLowerCase() });
  };

  changeCnpj = (cnpj) => {
    this.setState({ cnpj: cnpj });
  }

  render() {
    return (<View style={styles.container}>
      <TextInput
        autoComplete="name"
        placeholder="Nome"
        style={styles.formControl}
        value={this.state.name}
        onChangeText={(name) => this.changeName(name)} />
      <TextInput
        placeholder="Último Nome"
        style={styles.formControl}
        value={this.state.last_name}
        onChangeText={(last_name) => this.changeLastName(last_name)} />
      <TextInputMask
        type={'cnpj'}
        placeholder="CNPJ"
        style={styles.formControl}
        value={this.state.cnpj}
        onChangeText={(cnpj) => this.changeCnpj(cnpj)} />
      <TextInput
        autoComplete="email"
        placeholder="Email"
        style={styles.formControl}
        value={this.state.email}
        onChangeText={(email) => this.setState({ email: email })} />
      <TextInput
        placeholder="Nome de Usuário"
        style={styles.formControl}
        value={this.state.username}
        onChangeText={(username) => this.setState({ username: username })} />
      <TextInput
        placeholder="Senha"
        style={styles.formControl}
        value={this.state.password}
        onChangeText={(password) => this.setState({ password: password })}
        secureTextEntry={true} />
      <View style={styles.bottomView}>
        <TouchableOpacity style={styles.submit} onPress={() => { this.createUser() }}>
          <Text style={{ color: "#FFF", fontSize: 18, fontWeight: "bold" }}>Cadastrar Usuário</Text>
        </TouchableOpacity>
      </View>
      <Text style={{ color: "red" }}>{this.state.message}</Text>
    </View>)
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: "#FFF",
  },
  formControl: {
    height: 60,
    borderColor: "#FFF",
    borderBottomColor: "#FDD912",
    borderWidth: 2,
  },
  submit: {
    backgroundColor: "#FDD912",
    alignSelf: 'center',
    alignItems: 'center',
    padding: 20,
    width: "100%",
    marginBottom: Platform.OS === "ios" ? 20 : 0,
  },
  bottomView: {
    flex: 1,
    justifyContent: 'flex-end',
  },
});