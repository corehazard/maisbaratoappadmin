import React, { Component } from 'react';

import { View, Image, Text, StyleSheet } from 'react-native';

import success from '../resources/images/1x/success.png';


export default class Success extends Component {
  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    setTimeout(() => {
      this.props.navigation.navigate("Main");
    }, 3000);
  }

  render() {
    return (<View style={styles.container}>
      <Image source={success} style={styles.checkImage} />
      <Text style={styles.success}>Sucesso!</Text>
      <Text style={styles.message}>Agora o usuário pode cadastrar as ofertas.</Text>
    </View>);
  }
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#FDD912",
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -30,
  },
  checkImage: {
    width: "40%",
    resizeMode: "contain"
  },
  success: {
    color: "#FFF",
    fontSize: 30,
    fontWeight: "bold"
  },
  message: {
    color: "#FFF",
    fontSize: 16,
  }
});