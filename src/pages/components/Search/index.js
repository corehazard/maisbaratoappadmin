import React, { Component, Fragment } from 'react';

import { Platform, View, Image, StyleSheet } from 'react-native';

import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

import markerImage from '../../../resources/images/1x/+BMapMarkermdpi.png'

export default class Search extends Component {
  state = {
    searchFocused: false,
  }

  render() {
    const { onLocationSelected } = this.props;
    const { searchFocused } = this.state;

    return (<Fragment>
      <GooglePlacesAutocomplete
        placeholder="Digite o local do Estabelecimento"
        placeholderTextColor="#888"
        onPress={onLocationSelected}
        query={{
          key: "AIzaSyCMma8fQDCYP9uI8UHIvVNdJ4xzwTlyXeI",
          language: "pt"
        }}
        textInputProps={{
          onFocus: () => { this.setState({ searchFocused: true })},
          onBlur: () => { this.setState({ searchFocused: false })},
          autoCapitalize: "none",
          autoCorrect: false
        }}
        listViewDisplayed={searchFocused}
        fetchDetails
        enablePoweredByContainer={false}
        styles={{
          container: {
            position: "absolute",
            top: Platform.select({ ios: 20, android: 10 }),
            width: "100%"
          },
          textInputContainer: {
            flex: 1,
            backgroundColor: "transparent",
            height: 54,
            marginHorizontal: 20,
            borderTopWidth: 0,
            borderBottomWidth: 0
          },
          textInput: {
            height: 54,
            margin: 0,
            borderRadius: 0,
            paddingBottom: 0,
            paddingLeft: 20,
            paddingTop: 0,
            paddingRight: 20,
            marginTop: 0,
            marginLeft: 0,
            marginRight: 0,
            elevation: 5,
            shadowColor: "#000",
            shadowOpacity: 0.1,
            shadowOffset: { x: 0, y: 0 },
            shadowRadius: 15,
            borderWidth: 1,
            borderColor: "#DDD",
            fontSize: 18,
            color: "#888"
          },
          listView: {
            borderWidth: 1,
            borderColor: "#DDD",
            backgroundColor: "#FFF",
            marginHorizontal: 20,
            elevation: 5,
            shadowColor: "#000",
            shadowOpacity: 0.1,
            shadowOffset: { x: 0, y: 0 },
            shadowRadius: 15,
            marginTop: 10,
          },
          description: {
            fontSize: 16
          },
          row: {
            padding: 20,
            height: 58
          }
        }}
      />
      <View pointerEvents="none" style={styles.markerView}>
        <Image pointerEvents="none" style={styles.marker} source={markerImage} />
      </View>
    </Fragment>)
  }
}

const styles = StyleSheet.create({
  markerView: {
    position: 'absolute',
    top: 0,
    bottom: 30,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  marker: {
    maxWidth: 30,
    resizeMode: "contain"
  }
});
