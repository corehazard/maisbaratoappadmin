import React, { Component } from 'react';

import MapView from 'react-native-maps';

import { View, TouchableOpacity, Text, StyleSheet, Platform } from 'react-native';

import Search from './components/Search';

// import { Container } from './styles';

export default class MapSelector extends Component {
  static navigationOptions = {
    title: "Selecionar Localização",
    headerLeft: null,
  };

  state = {
    region: null,
    position: null
  };

  async componentDidMount() {
    navigator.geolocation.getCurrentPosition(
      ({ coords: { latitude, longitude } }) => {
        this.setState({
          region: {
            latitude,
            longitude,
            latitudeDelta: 0.0043,
            longitudeDelta: 0.0034
          }
        });
      },
      () => { },
      {
        timeout: 2000,
        enableHighAccuracy: true,
        maximumAge: 1000
      }
    )
  }

  handleLocationSelected = (data, { geometry }) => {
    const { location: { lat: latitude, lng: longitude } } = geometry;

    this.setState({
      region: {
        latitude,
        longitude,
        latitudeDelta: 0.0010747972260674032,
        longitudeDelta: 0.0009055808186602121
      }
    });
  }

  onRegionChange = (region) => {
    this.setState({
      position: region,
      region
    });
  }

  setLocation = () => {
    const { navigation } = this.props;
    const { region } = this.state;
    navigation.goBack();
    navigation.state.params.onSelect({ region });
  };

  render() {
    const { region } = this.state;

    return (<View style={{ flex: 1 }}>
      <MapView
        style={{ flex: 1 }}
        region={region}
        showsUserLocation={false}
        loadingEnabled
        onRegionChangeComplete={this.onRegionChange}
        ref={el => this.mapView = el}
      />
      <Search onLocationSelected={this.handleLocationSelected} />
      <View style={styles.bottomView}>
        <TouchableOpacity style={styles.selectLocation} onPress={() => { this.setLocation() }} >
          <Text style={{ color: "#FFF", fontSize: 18, fontWeight: "bold" }}>Selecionar Localização</Text>
        </TouchableOpacity>
      </View>
    </View>);
  }
}

const styles = StyleSheet.create({
  selectLocation: {
    backgroundColor: "#FDD912",
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    position: "absolute",
    bottom: 30,
    width: "90%",
    marginBottom: Platform.OS === "ios" ? 20 : 0,
  },
  bottomView: {
    justifyContent: 'flex-end',
  },
});