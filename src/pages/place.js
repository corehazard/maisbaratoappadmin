import React, { Component } from 'react';
import pnp_api from "../services/pnp_api";

import { View, Text, TextInput, TouchableOpacity, StyleSheet, Platform } from 'react-native';


export default class Place extends Component {
  static navigationOptions = {
    title: "Cadastrar Estabelecimento",
    headerLeft: null,
  };

  state = {
    claimant_id: "",
    name: "",
    description: "",
    latitude: "000",
    longitude: "000",
    message: "",
  };

  onSelect = data => {
    const { region } = data;
    this.setState({
      latitude: region.latitude,
      longitude: region.longitude
    });
  };

  componentDidMount() {
    this.setState({ claimant_id: this.props.navigation.getParam('userId', '') });
  }

  createPlace = async () => {
    try {
      if (
        this.state.name == "" ||
        this.state.description == "" ||
        this.state.latitude == "" || this.state.latitude == "000" ||
        this.state.longitude == "" || this.state.longitude == "000"
      ) {
        this.setState({ message: "Você deve preencher todos os campos e escolher a localização." });
        return;
      }


      const response = await pnp_api.post("/api/places", {
        claimant_id: this.state.claimant_id,
        name: this.state.name,
        description: this.state.description,
        location: {
          type: "Point",
          coordinates: [
            this.state.latitude,
            this.state.longitude
          ]
        }
      });

      const { id } = response.data;

      if (id.length === 0) {
        this.setState({ message: "Falha no cadastro de estabelecimento." })
      }
      else {
        this.props.navigation.navigate("Success");
      }
    }
    catch {
      this.setState({ message: "Erro ao cadastrar estabelecimento." })
    }
  }

  showMapSelector = () => {
    this.props.navigation.navigate("MapSelector", {
      onSelect: this.onSelect
    });
  }

  render() {
    const { latitude, longitude, message, name, description } = this.state;

    return (<View style={styles.container}>
      <TextInput
        placeholder="Nome do Estabelecimento"
        style={styles.formControl}
        value={name}
        onChangeText={(name) => this.setState({ name: name })} />
      <TextInput
        placeholder="Descrição"
        style={styles.formControl}
        value={description}
        onChangeText={(description) => this.setState({ description: description })} />
      <View style={styles.inlineContainer}>
        <View style={{ width: "70%", marginTop: 20 }}>
          <Text style={styles.coordsText}>Lat: {latitude}</Text>
          <Text style={styles.coordsText}>Lng: {longitude}</Text>
        </View>
        <TouchableOpacity style={styles.setCoords} onPress={() => { this.showMapSelector() }}>
          <Text style={{ color: "#FFF", fontSize: 18, fontWeight: "bold" }}>Localização</Text>
        </TouchableOpacity>
      </View>
      <Text style={{ color: "#FFF", fontSize: 18, fontWeight: "bold" }}>Cadastrar Usuário</Text>
      <View style={styles.bottomView}>
        <TouchableOpacity style={styles.submit} onPress={() => { this.createPlace() }}>
          <Text style={{ color: "#FFF", fontSize: 18, fontWeight: "bold" }}>Cadastrar Estabelecimento</Text>
        </TouchableOpacity>
      </View>
      <Text style={{ color: "red" }}>{message}</Text>
    </View>);
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: "#FFF",
  },
  inlineContainer: {
    flexDirection: "row",
    alignItems: "center",
    padding: 10
  },
  formControl: {
    height: 60,
    borderColor: "#FFF",
    borderBottomColor: "#FDD912",
    borderWidth: 2,
  },
  submit: {
    backgroundColor: "#FDD912",
    alignSelf: 'center',
    alignItems: 'center',
    padding: 20,
    width: "100%",
    marginBottom: Platform.OS === "ios" ? 20 : 0
  },
  bottomView: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  coordsText: {
    height: 30,
    marginTop: 5,
    fontSize: 18,
    color: "#BBB"
  },
  setCoords: {
    backgroundColor: "#FDD912",
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
    position: "absolute",
    right: 5,
    top: 30,
  }
});