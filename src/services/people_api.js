import Axios from "axios";

import {peopleBaseURL as base} from '../../app.json';

const people_api = Axios.create({
    baseURL: base
});

export default people_api;