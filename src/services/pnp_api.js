import Axios from "axios";

import {pnpBaseURL as base} from '../../app.json';

const pnp_api = Axios.create({
    baseURL: base
});

export default pnp_api;