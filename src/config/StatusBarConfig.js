import { StatusBar } from 'react-native';

StatusBar.setBackgroundColor("#E3C310");
StatusBar.setBarStyle('light-content');