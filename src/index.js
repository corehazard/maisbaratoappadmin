import React from 'react';

import { View } from 'react-native';
import './config/StatusBarConfig';

import Routes from './routes';

const App = () => <Routes />;

export default App;

